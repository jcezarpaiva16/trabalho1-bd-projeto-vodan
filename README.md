Script de criação do banco + população:
-----------------------------------------
```
DROP TABLE IF EXISTS `tb_grouprole`;
CREATE TABLE `tb_grouprole` (
  `groupRoleID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`groupRoleID`),
  UNIQUE KEY `groupRoleID` (`groupRoleID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

LOCK TABLES `tb_grouprole` WRITE;
INSERT INTO `tb_grouprole` VALUES (1,'Administrador');
UNLOCK TABLES;

DROP TABLE IF EXISTS tb_grouprolepermission;
CREATE TABLE tb_grouprolepermission (
  groupRoleID int(11) NOT NULL,
  permissionID int(11) NOT NULL,
  tableID int(11) NOT NULL,
  PRIMARY KEY (groupRoleID, permissionID, tableID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tb_grouprolepermission` WRITE;
INSERT INTO `tb_grouprolepermission` VALUES (1,4,1),(1,4,2),(1,4,3);
UNLOCK TABLES;

DROP TABLE IF EXISTS `tb_hospitalunit`;
CREATE TABLE `tb_hospitalunit` (
  `hospitalUnitID` int(10) NOT NULL AUTO_INCREMENT,
  `hospitalUnitName` varchar(500) NOT NULL COMMENT '(pt-br) Nome da unidade hospitalar.\r\n(en) Name of the hospital unit.',
  PRIMARY KEY (`hospitalUnitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='(pt-br) Tabela para identificação de unidades hospitalares.\r\n(en) Table for hospital units identification.';

LOCK TABLES `tb_hospitalunit` WRITE;
INSERT INTO `tb_hospitalunit` VALUES (NULL, 'Hospital Universitário Clementino Fraga Filho');
UNLOCK TABLES;

DROP TABLE IF EXISTS `tb_permission`;
CREATE TABLE `tb_permission` (
  `permissionID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`permissionID`),
  UNIQUE KEY `permissionID` (`permissionID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `tb_permission` WRITE;
INSERT INTO `tb_permission` VALUES (1,'Insert'),(2,'Update'),(3,'Delete'),(4,'ALL');
UNLOCK TABLES;

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `userID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `regionalCouncilCode` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `eMail` varchar(255) DEFAULT NULL,
  `foneNumber` varchar(255) DEFAULT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userID` (`userID`),
  UNIQUE KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tb_userrole`;
CREATE TABLE `tb_userrole` (
  `userID` int(11) NOT NULL,
  `groupRoleID` int(11) NOT NULL,
  `hospitalUnitID` int(11) NOT NULL,
  `creationDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `expirationDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userID`,`groupRoleID`,`hospitalUnitID`),
  KEY `FKtb_UserRol864770` (`groupRoleID`),
  KEY `FKtb_UserRol324331` (`hospitalUnitID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

```



Rotas
---------------------------------------------------------------------------------------------------------------------------------------------
Rotas genericas de verificação:
- ALL ```*``` -- -- verifica as permissões que o usuário para gerencial o hospital selecionado;
- ALL ```/users/*``` -- faz o controle de acesso à área de gerenciamento de usuários;
- ALL ```/roles/*``` -- faz o controle de acesso à área de gerenciamento de cargos e permissões;



Rotas de administração de usuários:
- GET ```/users/register``` -- renderiza o formulário de registro;
- POST ```/users/register``` -- obtem as informações provenientes do formulário de registro e registra um novo usuário;
- GET ```/users/list``` -- lista os usuários cadastrados (ativos e inativos) e disponibiliza opções de edição, inativação, ativação e registro de usuários;
- POST ```/users/status-change``` -- troca o status do usuário entre ativo e inativo (soft-delete);
- GET ```/users/role/list``` -- lista os cargos e alocação dos usuários;
- POST ```/users/role/find``` -- renderiza o formulário de edição de informações do usuário;
- POST ```/users/role/edit``` -- obtem as informações provenientes do formulário de edição e edita as informações do usuário;
- GET ```/users/role/insert``` -- renderiza o formulário de alocação de funcionário com um cargo para um hospital;
- POST ```/users/role/insert``` -- obtem as informações provenientes do formulário de alocação e aloca um usuário;
- POST ```/users/role/delete``` -- desaloca um funcionário de seu cargo em um hospital;
- POST ```/hospital-unit``` -- seleciona uma unidade hospitalar para administrar;



Rotas de autenticação e gestão individual de usuário:
- GET ```/delete``` -- (deprecada) executa um hard-delete do usuário;
- GET ```/profile``` -- desvia o usuário para página de login ou perfil de acordo com o status de autenticação;
- GET ```/``` -- desvia para ```/profile```;
- GET ```/login``` -- renderiza o formulário de login do usuário;
- POST ```/login``` -- obtem as informações provenientes do formulário de login e realiza autenticação do usuário caso os dados sejam válidos -- em caso negativo, retorna ao formulário de login exibindo mensagem de erro de acordo com o código de erro ocorrido;
- GET ```/logout``` -- encerra a sessão do usuário;
- POST ```/update``` -- atualiza as próprias informações de usuário que não são sensíveis;
- POST ```/update-password``` -- atualiza a senha do próprio usuário;



Rotas de administração de cargos e permissões:
- GET ```/roles/list``` -- lista os cargos e suas respectivas permissões e disponibiliza opções de edição, remoção e registro de cargos;
- GET ```/roles/find/:id``` -- renderiza o formulário de edição de permissões do cargo;
- POST ```/roles/editRole/``` -- obtem as informações provenientes do formulário de edição e edita as permissõesdo cargo;
- GET ```/roles/delete/:id``` -- deleta um cargo;
- GET ```/roles/register``` -- renderiza o formulário de criação de cargo;
- POST ```/roles/register``` -- obtem as informações provenientes do formulário de criação e cria um cargo com as respectivas permissões;



Rotas de administração de hospitais:
- GET ```/hospitals/list``` -- lista os hospitais e disponibiliza opções de edição, remoção e registro de hospitais;
- GET ```/hospitals/find/:id``` -- renderiza o formulário de edição de informações do hospital;
- POST ```/hospitals/find/:id``` -- obtem as informações provenientes do formulário de edição e edição um hospital;
- GET ```/hospitals/delete/:id``` -- deleta um hospital;
- GET ```/hospitals/register``` -- renderiza o formulário de registro de hospital;
- POST ```/hospitals/register``` -- obtem as informações provenientes do formulário de registro e registra um novo hospital;



Queries
----------------------------------------------------------------------------------------------------
Queries de administração de hospitais:
- ```SELECT * FROM tb_hospitalunit``` -- lista os hospitais registrados;
- ```SELECT * FROM tb_hospitalunit WHERE hospitalUnitID=?``` -- seleciona o hospital de acordo com o ID;
- ```UPDATE tb_hospitalunit SET ? WHERE hospitalUnitID=?``` -- atualiza os dados do hospital de acordo com o ID;
- ```INSERT INTO tb_hospitalunit (hospitalUnitName) VALUES (?)``` -- cria um novo hospital com o nome passado, alocando dinamicamente um novo ID;
- ```DELETE FROM tb_hospitalunit WHERE hospitalUnitID = ?``` -- deleta o hospital registrado de acordo com o ID;



Queries de autenticação e controle de permissões:
- ```SELECT a.userID, a.hospitalUnitID, GROUP_CONCAT(b.permissionID) AS permissionID, b.tableID FROM tb_userrole AS a INNER JOIN tb_grouprolepermission AS b WHERE a.groupRoleID = b.groupRoleID AND a.hospitalUnitID = ? AND a.userID = ? AND b.tableID = ? GROUP BY a.userID, a.hospitalUnitID, b.tableID``` -- verificaca as permissões de um usuário de acordo com o ID do usuário e o ID do hospital para manipular os dados de uma área (usuários, hospitais, cargos) especificada por ID;
- ```SELECT a.userID, a.hospitalUnitID, GROUP_CONCAT(b.permissionID) as permitions FROM ( tb_userrole AS a LEFT JOIN tb_grouprolepermission AS b ON a.groupRoleID = b.groupRoleID ) WHERE a.userID = ? GROUP BY a.userID, a.hospitalUnitID``` -- lista as permissões de um usuário para cada hospital;
- ```SELECT * FROM tb_hospitalunit WHERE hospitalUnitID IN(?)``` -- lista as unidades hospitalares para um grupo de hospitais determinado por IDs;
- ```SELECT * FROM tb_grouprole``` -- retorna a tabela de cargos;



Queries de administração de usuários:
- ```SELECT * FROM tb_User WHERE login = ?``` -- seleciona usuário por login;
- ```SELECT login FROM tb_user WHERE login = ?``` -- seleciona login de usuário por login (para verificação durante o registro)
- ```INSERT INTO tb_User SET ?``` -- registra um novo usuário no banco de dados;
- ```INSERT INTO tb_userrole (userID, groupRoleID, hospitalUnitID, creationDate, expirationDate) VALUES (?, ?, ?, ?, NULL)``` -- registra um cargo para um usuário;
- ```SELECT * FROM tb_user``` -- retorna a tabela de usuários;
- ```SELECT * FROM tb_user WHERE userID=?``` -- seleciona um usuário pelo ID;
- ```UPDATE tb_user SET ? WHERE userID=?``` -- edita os dados de um usuário pelo ID;
- ```SELECT a.userID, a.login FROM tb_user AS a``` -- seleciona ID e login de todos os usuários do banco;
- ```UPDATE tb_userrole SET expirationDate = NULL, groupRoleID = ?, hospitalUnitID = ? WHERE userID = ? AND groupRoleID = ? AND hospitalUnitID = ?``` -- edita a alocação de um usuário;
- ```INSERT INTO `tb_userrole` (`userID`, `groupRoleID`, `hospitalUnitID`, `creationDate`, `expirationDate`) VALUES (?, ?, ?, ?, NULL)``` -- aloca um usuário com um cargo em uma unidade hospitalar;
- ```SELECT * FROM tb_userrole WHERE userID = ?``` -- retorna os todos dados de alocação de um usuário para um ID;
- ```DELETE FROM tb_userrole WHERE userID = ? AND groupRoleID = ? AND hospitalUnitID = ?``` -- desaloca um usuário;
- ```SELECT deleted_at FROM tb_User WHERE userID = ?``` -- coleta o status do usuário especificado por ID;
- ```UPDATE tb_User SET deleted_at = ? WHERE userID = ?``` -- atualiza o status do usuário especificado por ID (aplica ou desaplica um soft-delete);
- ```DELETE FROM tb_User WHERE userID = ?``` -- aplica um hard-delete;
- ```UPDATE tb_User SET password = ?  WHERE userID = ?``` -- atualiza a senha de um usuário;



Queries de administração de cargos e permissões:
- ```DELETE FROM tb_GroupRolePermission WHERE groupRoleID = ?``` -- deleta as permissões de um cargo especificado por ID;
- ```DELETE FROM tb_grouprole WHERE groupRoleID = ?``` -- deleta um cargo especificado por ID;
- ```INSERT INTO tb_grouprolepermission (groupRoleID, permissionID, tableID) VALUES (?, ?, ?)``` -- insere uma permissão especificada por ID de uma área (usuários, hospitais, cargos) especificada por ID para um cargo especificado por ID;
- ```INSERT INTO tb_grouprole (groupRoleID, description) VALUES (?, ?)``` -- cria um novo cargo especificando o ID (é utilizada somente no processo de edição de permissões);
- ```INSERT INTO tb_grouprole (description) VALUES (?)``` -- cria um novo cargo, alocando dinamicamente o ID;

