const http = require('http');

//------------------------------------
// configura e começa a connection
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'mysql',
  database : 'trabalho'
});

connection.connect();

//------------------------------------
const hostname = 'localhost';
const port = 3000;

let bananinha;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  connection.query({
    sql: 'SHOW TABLES',
    timeout: 40000, // 40s
  }, function (error, results, fields) {
    // error will be an Error if one occurred during the query
    // results will contain the results of the query
    // fields will contain information about the returned results fields (if any)
        bananinha = results;
    });
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});


bananinha;

//-----------------------------------
// finaliza a connection
// connection.end();